﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using System.Reflection;

namespace Otus.Teaching.PromoCodeFactory.Sqlite
{
    public class DataContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            var assemblyName = typeof(DataContextFactory).GetTypeInfo().Namespace;
            var optionsBuilder = new DbContextOptionsBuilder<DataContext>();

            optionsBuilder.UseSqlite("Data Source=PromoCodeFactory.sqlite",
                 x => x.MigrationsAssembly(assemblyName));

            return new DataContext(optionsBuilder.Options);
        }
    }
}
