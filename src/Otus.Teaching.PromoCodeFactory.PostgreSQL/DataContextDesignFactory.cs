﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using System.Reflection;

namespace Otus.Teaching.PromoCodeFactory.PostgreSQL
{
    public class DataContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            var assemblyName = typeof(DataContextFactory).GetTypeInfo().Namespace;
            var optionsBuilder = new DbContextOptionsBuilder<DataContext>();

            optionsBuilder
                .UseNpgsql("Host=localhost:5434;Database=PromoCodeFactory;Username=otus;Password=otus",
                x => x.MigrationsAssembly(assemblyName));

            return new DataContext(optionsBuilder.Options);
        }
    }
}
