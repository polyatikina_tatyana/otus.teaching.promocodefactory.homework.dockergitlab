﻿namespace Otus.Teaching.PromoCodeFactory.DataProviders
{
    public static class DataProviderKind
    {
        public const string Sqlite = nameof(Sqlite);
        public const string PostgreSql = nameof(PostgreSql);
    }
}
