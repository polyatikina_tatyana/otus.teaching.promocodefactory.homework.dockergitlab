﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using System;
using System.Reflection;

namespace Otus.Teaching.PromoCodeFactory.DataProviders
{
    public static class StartupExtensions
    {
        public static IServiceCollection AddDataProvider(this IServiceCollection services, IConfiguration configuration)
        {            
            services.AddDbContext<DataContext>(
                    options =>
                    {
                        var provider = configuration.GetConnectionString("ProviderName");
                        var connectionString = configuration.GetConnectionString("ProviderConnection");

                        _ = provider switch
                        {
                            DataProviderKind.Sqlite => options.UseSqlite(connectionString,
                                x => x.MigrationsAssembly(typeof(Sqlite.DataContextFactory).GetTypeInfo().Namespace)),

                            DataProviderKind.PostgreSql => options.UseNpgsql(connectionString,
                                x => x.MigrationsAssembly(typeof(PostgreSQL.DataContextFactory).GetTypeInfo().Namespace)),

                            _ => throw new Exception($"Unsupported provider: {provider}")
                        };

                        options.UseLazyLoadingProxies();
                    });

            return services;
        }
    }
}
